from django.db.models.fields.related import ForeignKey
from django.db.models.signals import m2m_changed, post_init, pre_save, post_save, post_delete
from django.core.exceptions import ObjectDoesNotExist
from django.dispatch import receiver
from tyufyakin.models import DefaultModel
from django.contrib.auth.models import User

User._meta.get_field_by_name('email')[0]._unique = True


@receiver(m2m_changed)
def on_m2m_changed(sender, **kw):
    method_name = '%s_%s' % (sender.__name__.split('_', 1)[-1], kw['action'])
    if hasattr(kw['instance'], method_name):
        getattr(kw['instance'], method_name)(list(kw['pk_set']), **kw)


@receiver(post_init)
def on_post_init(sender, instance, **kw):
    if issubclass(sender, DefaultModel):
        instance.configured_fields = {}
        instance.modified_fields = {}
        if instance.id is None:
            for field in instance._meta.fields:
                instance.modified_fields[field.name] = None


@receiver(pre_save)
def on_pre_save(sender, instance, **kw):
    if issubclass(sender, DefaultModel):
        if hasattr(instance, 'pre_save'):
            kw['creating'] = instance.id is None
            getattr(instance, 'pre_save')(**kw)


@receiver(post_save)
def on_post_save(sender, instance, **kw):
    if issubclass(sender, DefaultModel):
        if hasattr(instance, 'post_save'):
            instance.post_save(**kw)
        for field in instance._meta.fields:
            if type(field) is ForeignKey:
                value = getattr(instance, field.name)
                related_name = field.rel.related_name
                if related_name is not None:
                    if field.name in instance.modified_fields:
                        old_value = instance.modified_fields.get(field.name)
                        if old_value is not value:
                            if old_value is not None:
                                method_name = '%s_post_delete' % related_name
                                if hasattr(old_value, method_name):
                                    getattr(old_value, method_name)(instance)
                            if value is not None:
                                method_name = '%s_post_add' % related_name
                                if hasattr(value, method_name):
                                    getattr(value, method_name)(instance, **kw)
                    if value is not None:
                        method_name = '%s_post_save' % related_name
                        if hasattr(value, method_name):
                            getattr(value, method_name)(instance)
        instance.configured_fields = {}
        instance.modified_fields = {}


@receiver(post_delete)
def on_post_delete(sender, instance, **kw):
    if issubclass(sender, DefaultModel):
        if hasattr(instance, 'post_delete'):
            instance.post_delete(**kw)
        for field in instance._meta.fields:
            if type(field) is ForeignKey:
                try:
                    value = getattr(instance, field.name)
                    related_name = field.rel.related_name
                    if related_name is not None:
                        if value is not None:
                            method_name = '%s_post_delete' % related_name
                            if hasattr(value, method_name):
                                getattr(value, method_name)(instance)
                except ObjectDoesNotExist:
                    pass  # related object may be already delete
