# coding=utf-8
# from accounts.models import penalize
from django.contrib import admin
from django.conf import settings
from tyufyakin.models import *


class ModelAdmin(admin.ModelAdmin):
    actions = ['custom_delete_selected']

    def custom_delete_selected(modeladmin, request, queryset):
        for obj in queryset:
            obj.delete()

    def get_actions(self, request):
        actions = super(ModelAdmin, self).get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def delete_model(self, request, obj):
        penalize(obj, request.user)
        obj.delete()

    custom_delete_selected.short_description = u"Удалить выбранные объекты"


class RecordAdmin(admin.ModelAdmin):
    fields = ('first_summand', 'second_summand', 'result', 'has_error')

admin.site.register(Record, RecordAdmin)

