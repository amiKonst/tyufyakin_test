# coding=utf-8
from django.core.cache import cache
from hashlib import md5
from django.utils.http import urlquote


def get_cache_for(field, obj, time=1200):
    cached = cache.get(field)
    if cached is not None:
        result = cached
    else:
        result = obj
        cache.set(field, obj, time)
    return result


def get_template_cache_key(key, variables=list()):
    """
    Возвращает ключ, по которому можно получить кэш, созданный джанговским шаблонным тегом cache
    """
    hashed = md5(u':'.join([urlquote(var) for var in variables]))
    key = 'template.cache.%s.%s' % (key, hashed.hexdigest())
    return key


def get_template_cache(key, variables=list()):
    """
    Возвращает указанный шаблонный кэш
    """
    key = get_template_cache_key(key, variables)
    template_cache = cache.get(key)
    return template_cache


def clear_template_cache(key, variables=list()):
    """
    Удаляет указанный шаблонный кэш
    """
    key = get_template_cache_key(key, variables)
    cache.delete(key)
