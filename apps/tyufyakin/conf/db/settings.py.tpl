DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '{{ db.name }}',
        'USER': '{{ db.user }}',
        'PASSWORD': '{{ db.password }}',
        'HOST': 'localhost',
        'PORT': '5432'
    },
    'migration': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': "migrate",
        'USER': '{{ db.user }}',
        'PASSWORD': '{{ db.password }}',
        'HOST': 'localhost',
        'PORT': '5432'
    }
}