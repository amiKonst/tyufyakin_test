PROJECT_ROOT = '/vagrant'


DEBUG = True

TEMPLATE_DEBUG = DEBUG
LOGGING_CONFIG = None


STATIC_ROOT = PROJECT_ROOT + '/static/'
STATIC_URL = '/static/'

MEDIA_ROOT = STATIC_ROOT + 'media/'
MEDIA_URL = STATIC_URL + 'media/'


TIME_ZONE = 'Europe/Moscow'
LANGUAGE_CODE = 'ru'
SITE_ID = 1

USE_I18N = True
USE_L10N = True
USE_TZ = False

ROOT_URLCONF = 'tyufyakin.urls'
DATE_FORMAT = '%d/%m/%Y'
TIME_FORMAT = '%H:%M:%S'
DATETIME_FORMAT = DATE_FORMAT + ' ' + TIME_FORMAT

SITE_URL = 'tyufyakin.com'


TEMPLATE_LOADERS = (
                       'django.template.loaders.cached.Loader', (
                           'django.template.loaders.filesystem.Loader',
                           'django.template.loaders.app_directories.Loader',
                       )
),

TEMPLATE_DIRS = (
    '/vagrant/templates/',
    'django/contrib/admin/templates/',
)

FIXTURE_DIRS = (
    '/vagrant/tyufyakin/fixtures/',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    'tyufyakin.middleware.AjaxResponseMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.request',
    'django.core.context_processors.static',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'tyufyakin.context_processors.libs',
    'tyufyakin.context_processors.default_values',
    'social_auth.context_processors.social_auth_by_type_backends'
)

AUTH_PROFILE_MODULE = 'accounts.UserProfile'
ACCOUNT_ACTIVATION_DAYS = 7


EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
EMAIL_FILE_PATH = PROJECT_ROOT + '/tmp/email'


CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}
PER_PAGE = 15

AUTHENTICATION_BACKENDS = (
    'social_auth.backends.twitter.TwitterBackend',
    'social_auth.backends.facebook.FacebookBackend',
    'social_auth.backends.google.GoogleOAuth2Backend',
    'social_auth.backends.contrib.vkontakte.VKontakteOAuth2Backend',
    'django.contrib.auth.backends.ModelBackend',
)

SOCIAL_AUTH_PIPELINE = (
    'social_auth.backends.pipeline.social.social_auth_user',
    'social_auth.backends.pipeline.user.get_username',
    'social_auth.backends.pipeline.user.create_user',
    'social_auth.backends.pipeline.social.associate_user',
    'social_auth.backends.pipeline.social.load_extra_data',
    'social_auth.backends.pipeline.user.update_user_details',
    'accounts.social.load_profile_data',
)


INSTALLED_APPS = (    
    'django.contrib.auth',
    'django.contrib.sites',
    'django.contrib.admin',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.staticfiles',
    'django.contrib.flatpages',
    'django.contrib.messages',
    'django.contrib.humanize',
    'django.contrib.sitemaps',
    'djcelery',
    'south',
    'json_field',
    'tyufyakin'
)
SOUTH_TESTS_MIGRATE = False
INTERNAL_IPS = ('127.0.0.1',)
SPHINX_API_VERSION = 0x116
DEBUG_TOOLBAR_PANELS = (
    'debug_toolbar.panels.version.VersionDebugPanel',
    'debug_toolbar.panels.timer.TimerDebugPanel',
    'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
    'debug_toolbar.panels.headers.HeaderDebugPanel',
    'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
    'debug_toolbar.panels.template.TemplateDebugPanel',
    'debug_toolbar.panels.sql.SQLDebugPanel',
    'debug_toolbar.panels.signals.SignalDebugPanel',
    'debug_toolbar.panels.logger.LoggingPanel',
)

from web.settings import *
from db.settings import *
from supervisor.celery_settings import *

DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
}