# coding=utf-8
import djcelery
djcelery.setup_loader()

from celery.schedules import crontab

CELERY_TIMEZONE = 'Europe/Moscow'
CELERYBEAT_SCHEDULER = 'djcelery.schedulers.DatabaseScheduler'
BROKER_URL = 'amqp://guest:guest@localhost:5672/'
CELERY_IMPORTS = (
    'tyufyakin.tasks'
)


CELERYBEAT_SCHEDULE = {
    'dump_database_every_night': {
        'task': 'tyufyakin.tasks.dumpdb',
        'schedule': crontab(minute=0, hour=1)
    },
}