LIB_URLS = {
    'head': '{{ web.lib_url }}/head/0.99/',
    'jquery': '{{ web.lib_url }}/jquery/1.8.2/',
    'jquery_ui': '{{ web.lib_url }}/jquery-ui/1.10.3/',
    'jquery_colorbox': '{{ web.lib_url }}/jquery-colorbox/1.3.19.3/',
    'jquery_sparkline': '{{ web.lib_url }}/jquery-sparkline/2.1.1/',
    'bootstrap': '{{ web.lib_url }}/bootstrap/2.1.1/',
    'jasmine': '{{ web.lib_url }}/jasmine/1.2.0/',
    'jasmine_jquery': '{{ web.lib_url }}/jasmine-jquery/',
    'knockout': '{{ web.lib_url }}/knockout/2.2.0/',
    'knockout_mapping': '{{ web.lib_url }}/knockout-mapping/2.3.5/',
    'ckeditor': '{{ web.lib_url }}/ckeditor/4.2.0/',
    'momentjs': '{{ web.lib_url }}/momentjs/2.0.0/',
    'highstock': '{{ web.lib_url }}/highstock/1.3.2/',
    'jquery_sparkline': '{{ web.lib_url }}/jquery-sparkline/2.1.1/',
    'jquery_cycle': '{{ web.lib_url }}/jquery-cycle/3.0.1/',
    'jquery_cookie': '{{ web.lib_url }}/jquery-cookie/',
    'zeroclipboard': '{{ web.lib_url }}/zeroclipboard/1.0.7/',

    'jquery_imgareaselect': '{{ web.lib_url }}/jquery-imgareaselect/',
    'jquery_mobile': '{{ web.lib_url }}/jquery-mobile/1.3.2/',
    'jquery_jscrollpane': '{{ web.lib_url }}/jquery-jscrollpane/2.0.0/',
    'jquery_mousewheel': '{{ web.lib_url }}/jquery-mousewheel/3.1.6/',
    'jquery_tooltipster': '{{ web.lib_url }}/jquery-tooltipster/',
    'jquery_autosize': '{{ web.lib_url }}/jquery-autosize/1.18.1/',
    'jquery_swiper': '{{ web.lib_url }}/jquery-swiper/',
    'timepicker': '{{ web.lib_url }}/timepicker/1.3/'

}