import os
import sys

path = '{{ project_root }}/apps'  # path to your working directory.
if path not in sys.path:
    sys.path.append(path)

os.environ['DJANGO_SETTINGS_MODULE'] = 'tyufyakin.conf.settings'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
