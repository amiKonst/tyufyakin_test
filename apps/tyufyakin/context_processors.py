# coding=utf-8
from django.conf import settings


def libs(request):
    context_extras = {'LIB_URLS': settings.LIB_URLS}
    return context_extras


def default_values(request):
    """Returns default values"""
    return {
        'defaults': {
            # 'anonym': 'Анонимный пользователь'
        }
    }
