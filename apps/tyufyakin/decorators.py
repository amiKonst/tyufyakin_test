from functools import wraps
from django.conf import settings

def only_for_production(func):

    @wraps(func)
    def wrapped(*args, **kwargs):
        if settings.PROJECT_ROOT == '/vagrant':
            return False
        else:
            return func(*args, **kwargs)

    return wrapped