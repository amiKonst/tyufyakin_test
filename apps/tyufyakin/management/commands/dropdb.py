# coding=utf-8
import sys
from django.core.management.base import BaseCommand
from django.conf import settings
from django.db import connection


class Command(BaseCommand):
    help = 'Drop current database and creates empty one.'
    requires_model_validation = False

    def handle(self, *labels, **options):
        sys.stdout.write("Reseting project %s\n" % settings.DATABASES['default']['NAME'])
        sys.stdout.flush()

        try:
            sys.stdout.write("Recreating schema 'public'... ")
            sys.stdout.flush()
            cur = connection.cursor()
            cur.execute('DROP SCHEMA public CASCADE;')
            cur.execute('CREATE SCHEMA public;')
            connection._commit()
            sys.stdout.write("Done\n")
            sys.stdout.flush()

        except:
            sys.stdout.write('FAIL\n')
            sys.stdout.flush()
            raise
