# coding=utf-8
import yaml
from optparse import make_option
from django.core.management.base import BaseCommand
from django.template import Context, Template


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--template', help='Template file path.'),
        make_option('--output', help='Output file path.'),
        make_option('--context', help='File path, containing context data in YAML.')
    )
    help = 'Renders template file using django template engine.'
    requires_model_validation = False

    def handle(self, *labels, **options):
        context_filename = options.get('context', None)
        template_filename = options.get('template')
        output_filename = options.get('output')
        if context_filename is not None:
            with open(context_filename, 'r') as context_file:
                context = Context(yaml.load(context_file.read()))
        else:
            context = Context({})
        with open(template_filename, 'r') as template_file:
            template = Template(template_file.read())
        with open(output_filename, 'w+') as output_file:
            output_file.write(template.render(context))
