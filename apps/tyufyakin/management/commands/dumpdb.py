# coding=utf-8
import sys
import os
import datetime
import subprocess
import tarfile
from optparse import make_option
from django.core.management.base import BaseCommand
from django.conf import settings


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--dir', help='Directory to store dump files.', default=os.getcwd()),
    )
    help = 'Create full database dump.'
    requires_model_validation = False

    def handle(self, *labels, **options):
        sys.stdout.write('Exporting project %s\nDumping... ' % settings.DATABASES['default']['NAME'])
        sys.stdout.flush()

        try:
            dump_file_name = datetime.datetime.now() \
                .strftime('%Y-%m-%d_%H:%M_' + settings.DATABASES['default']['NAME'] + '.sql')
            current_dir = os.getcwd()
            export_dir = options.get('dir')
            os.chdir(export_dir)
            dump_process = subprocess.Popen([
                'pg_dump', settings.DATABASES['default']['NAME'],
                '-U', 'postgres',
                '-f', dump_file_name
            ], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            dump_process.wait()
            if dump_process.returncode != 0:
                sys.stdout.write('FAIL\n' + dump_process.communicate()[1])
                sys.stdout.flush()
                return
            tarball = tarfile.open(dump_file_name + '.tar.gz', mode='w:gz')
            tarball.add(dump_file_name)
            tarball.close()
            os.remove(dump_file_name)
            os.chdir(current_dir)
            sys.stdout.write('Done (saved in ' + tarball.name + ')\n')
            sys.stdout.flush()

        except:
            sys.stdout.write('FAIL\n')
            sys.stdout.flush()
            raise
