# coding=utf-8
import sys
import os
import subprocess
import tarfile
from optparse import make_option
from django.core.management import call_command
from django.core.management.base import BaseCommand
from django.conf import settings


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--rm-source', help='Remove source file after loading.', default=False),
    )
    help = 'Load database from dump.'
    requires_model_validation = False

    def handle(self, *args, **options):
        sys.stdout.write('Importing project %s\n' % settings.DATABASES['default']['NAME'])
        sys.stdout.flush()
        call_command('dropdb')
        sys.stdout.write('Loading data... ')
        sys.stdout.flush()
        try:
            dump_file_name = args[0]
            if dump_file_name.endswith('.tar.gz'):
                tarball = tarfile.open(dump_file_name, "r:gz")
                current_dir = os.getcwd()
                working_dir = os.path.dirname(os.path.abspath(dump_file_name))
                os.chdir(working_dir)
                tarball.extractall()
                tarball.close()
                os.chdir(current_dir)
                dump_file_name = dump_file_name.rstrip('.tar.gz')

            dump_file = open(dump_file_name)
            loading_process = subprocess.Popen([
                'psql', settings.DATABASES['default']['NAME'],
                '-U', 'postgres'
            ], stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=dump_file)
            error_msg = loading_process.communicate()[1]
            dump_file.close()

            if error_msg:
                log = open('import.error.log', 'w')
                log.write(error_msg)
                log.close()
                sys.stdout.write('FAIL (some errors occured due to importing; see import.error.log for details)\n')
                sys.stdout.flush()
                return

            if options.get('rm-source'):
                os.remove(dump_file_name)
            os.chdir(current_dir)
            sys.stdout.write('Done\n')
            sys.stdout.flush()

        except:
            sys.stdout.write('FAIL\n')
            sys.stdout.flush()
            raise
