from django.core.management.base import BaseCommand, CommandError
from tyufyakin.tasks import migrate
from optparse import make_option


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--limit',
            action='store',
            type='int',
            help='Delete poll instead of closing it'),
        )
    def handle(self, *args, **options):
        migrate(**options)