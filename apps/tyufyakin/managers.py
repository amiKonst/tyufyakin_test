#coding=utf-8
from django.db import models
from tyufyakin.models import DefaultQuerySet


class DefaultManager(models.Manager):
    def get_query_set(self):
        return DefaultQuerySet(self.model)

    def editable_by(self, user, *args):
        return self.get_query_set().editable_by(user, *args)

    def viewable_by(self, user, *args):
        return self.get_query_set().viewable_by(user, *args)

    def is_editable_by(self, user, *args):
        return True
