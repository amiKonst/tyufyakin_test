import datetime
from django.http import HttpResponseNotFound, HttpResponseServerError
from django.template import loader, RequestContext
from django.conf import settings
from django.db import connection
# from accounts.models import UserLastActivity


class AjaxResponseMiddleware(object):

    def process_request(self, request):
        for action in ('/create/', '/read/', '/update/', '/destroy/'):
            if action in request.path:
                request.META['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest'
                break
        return None

    def process_response(self, request, response):
        if request.is_ajax():
            response['Content-Type'] = 'application/javascript'

        elif request.method == "GET":
            if response.status_code == 404:
                template_name = '404.html'
                template = loader.get_template(template_name)
                return HttpResponseNotFound(template.render(RequestContext(request, {})))
            elif response.status_code == 500 and settings.DEBUG is False:
                template_name = '500.html'
                template = loader.get_template(template_name)
                context = {}
                if response.content:
                    context['error_message'] = response.content
                return HttpResponseServerError(template.render(RequestContext(request, context)))
        return response

