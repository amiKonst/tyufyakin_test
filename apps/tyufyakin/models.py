# coding=utf-8
import json
import datetime
import ho.pisa as pisa
import cStringIO as StringIO
from django.db import models
from django.db.models import Sum, Count, Avg
from django.conf import settings
from django.template.loader import get_template
from django.template import Context
from django.core.files.uploadedfile import SimpleUploadedFile
from django.db.models.fields.related import ManyToManyField

models.options.DEFAULT_NAMES = tuple(list(models.options.DEFAULT_NAMES) + ['essential_fields'])

class ValidationError(Exception):
    """
    Класс, возвращающий ошибку валидации
    """
    def encode(self):
        if not isinstance(self.message, str):
            self.message = json.dumps(self.message)
        return self.message


class DefaultModel(models.Model):
    """
    Базовая модель. Определены основные параметры и функции.
    """

    class Meta:
        abstract = True
        essential_fields = ['id']

    def __setattr__(self, name, value):
        if hasattr(self, 'modified_fields') and hasattr(self, name):
            old_value = getattr(self, name)
            if old_value != value:
                if name not in self.modified_fields:
                    self.modified_fields[name] = old_value
                elif self.modified_fields[name] == value:
                    self.modified_fields.pop(name)
        return object.__setattr__(self, name, value)

    def encode(self, *args, **kw):
        return json.dumps(self.get(*args, **kw))

    def is_modified(self, *fields):
        if len(fields) > 0:
            for field in fields:
                if field in self.modified_fields:
                    return True
            return False
        else:
            return field in self.modified_fields

    def get_initial(self, field):
        return self.modified_fields[field]

    def drop_modified(self, field):
        return self.modified_fields.pop(field)

    def set(self, **kw):
        for field, value in kw.items():
            field_cls = self._meta.get_field(field)
            if type(field_cls) is models.fields.DateField and type(value) is not datetime.date:
                try:
                    value = datetime.datetime.strptime(value, settings.DATE_FORMAT)
                except (ValueError, TypeError):
                    value = None
            elif type(field_cls) is models.fields.DateTimeField and type(value) is not datetime.datetime:
                value = datetime.datetime.strptime(value, settings.DATETIME_FORMAT)
            elif type(field_cls) in (models.fields.related.ForeignKey, models.fields.related.OneToOneField) and not \
                    isinstance(type(value), models.Model) and value:
                    value = field_cls.rel.to.objects.get(id=value)
            elif type(field_cls) is ManyToManyField:
                if not type(value) is list:
                    raise TypeError('value must be an array')
                m2m_field = getattr(self, field)
                m2m_field.clear()
                for object_id in value:
                    m2m_field.add(m2m_field.model.objects.get(id=object_id))
            if not type(field_cls) is ManyToManyField:
                setattr(self, field, value)
        return self

    def get(self, *args, **kw):
        for field in args:
            if field not in kw:
                kw[field] = getattr(self, field)
                field_cls = self._meta.get_field(field)
                if type(field_cls) is models.fields.DateField:
                    kw[field] = kw[field].strftime(settings.DATE_FORMAT) if kw[field] else ''
                if type(field_cls) is models.fields.DateTimeField:
                    kw[field] = kw[field].strftime(settings.DATETIME_FORMAT) if kw[field] else ''
                if issubclass(type(field_cls), models.fields.files.FileField):
                    kw[field] = kw[field].url if kw[field] else ''
        return kw if len(kw.values()) != 1 else kw.values()[0]

    def get_essential_fields(self):
        return self.get(*self._meta.essential_fields)


class ValidatableModel(DefaultModel):
    """
    Наследуется от базовой модели **DefaultModel**.
    Добавлена валидация данных.
    """

    class Meta(DefaultModel.Meta):
        abstract = True

    def pre_save(self, **kw):
        self.validate()

    def validate(self, **kw):
        errors = self.validator(**kw)
        if errors:
            raise ValidationError(errors)
        return True

    def validator(self, **kw):
        raise NotImplementedError


class DefaultQuerySet(models.query.QuerySet):
    def editable_by(self, user):
        return self

    def viewable_by(self, user):
        return self

    def get_values_query_set(self):
        return models.query.ValuesQuerySet

    def create(self, **kwargs):
        configured_fields = kwargs.pop('configured_fields', {})
        for name, value in kwargs.items():
            if '__' in name:
                kwargs.pop(name)
                name = name.split('__', 1)
                configured_fields.setdefault(name[0], {})
                configured_fields[str(name[0])][str(name[1])] = value
            else:
                field = self.model._meta.get_field(name)
                if type(field) is models.fields.DateField and type(value) is not datetime.date:
                    kwargs[name] = datetime.datetime.strptime(value, settings.DATE_FORMAT)
                if type(field) is models.fields.DateTimeField and type(value) is not datetime.datetime:
                    kwargs[name] = datetime.datetime.strptime(value, settings.DATETIME_FORMAT)
        for name, config in configured_fields.items():
            try:
                field = self.model._meta.get_field(name)
                if type(field) in (models.fields.related.ForeignKey, models.fields.related.OneToOneField):
                    kwargs[name] = field.rel.to.objects.get_or_create(**config)[0]
                    configured_fields.pop(name)
            except models.fields.FieldDoesNotExist:
                pass
        object = self.model(**kwargs)
        object.configured_fields = configured_fields
        self._for_write = True
        object.save(force_insert=True, using=self.db)
        return object

    def get_or_create(self, **kwargs):
        defaults = kwargs.pop('defaults', {})
        try:
            return self.get(**kwargs), False
        except self.model.DoesNotExist:
            kwargs.update(defaults)
            return self.create(**kwargs), True

    def values(self, *fields):
        aggregate_functions = {'sum': Sum, 'count': Count, 'avg': Avg}
        fields = list(fields)
        annotates = []
        for i in range(len(fields) - 1, -1, -1):
            field = fields[i]
            lookup = field.split('__')
            if len(lookup) > 1:
                pop = lookup.pop()
                func = aggregate_functions.get(pop, None)
                if func is not None:
                    fields.pop(i)
                    annotates.append({
                        str(field): func('__'.join(lookup), distinct=True)
                    })
        queryset = self._clone(klass=self.get_values_query_set(), setup=True, _fields=fields)
        for annotate_config in annotates:
            queryset = queryset.annotate(**annotate_config)
        return queryset

    def disjunction_filters(self, lookup={}):
        """
        Interpret list of passed filters as logical OR lookups
        """
        if type(lookup) in (list, tuple):
            return reduce(
                lambda x, y: x | y,
                [self.filter(**i) for i in lookup]
            )
        return self.filter(**lookup)



class Record(ValidatableModel):
    """
    Модель для хранения записей выражений.
    """
    first_summand = models.FloatField(null=True, verbose_name=u'Слогаемое a')
    second_summand = models.FloatField(null=True, verbose_name=u'Слогаемое b')
    result = models.FloatField(null=True, blank=True, verbose_name=u'Результат')
    last_modified_date = models.DateTimeField(auto_now=True)
    has_error = models.BooleanField(default=False, verbose_name=u'Ошибка при выполнении')

    class Meta(ValidatableModel.Meta):
        verbose_name = u'Запись'
        verbose_name_plural = u'Записи'

    def __unicode__(self):
        return str(self.id)

    def validator(self, **kw):
        return None

    def is_editable_by(self, user):
        return True

    def is_viewable_by(self, user):
        return True