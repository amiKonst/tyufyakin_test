import os
import sys
import requests, json
from celery import group

path = '/var/www/tyufyakin/apps' 
if path not in sys.path:
    sys.path.append(path)
os.environ['DJANGO_SETTINGS_MODULE'] = 'tyufyakin.conf.settings'

from celery import Celery
from tyufyakin.models import Record

celery = Celery()
celery.config_from_object({
    'BROKER_URL': 'amqp://localhost',
    'CELERY_RESULT_BACKEND': 'amqp://',
    'CELERYD_POOL_RESTARTS': True,  # Required for /worker/pool/restart API
})


@celery.task
def assume():
    records = Record.objects.filter(result__isnull=True)
    group(add.s(item.id, item.first_summand, item.second_summand) for item in records)()
    return 'OK'


@celery.task
def add(id, x, y):
    ###
    # ...
    res = x + y
    ###
    save.s(id, res).apply_async()
    return 'OK'


@celery.task
def save(id, res):
    # m.b. has_error -> task res state?
    try:
        Record.objects.filter(id=id).update(result=res, has_error=False)
        return 'OK'
    except:
        Record.objects.filter(id=id).update(has_error=True)
        return 'FAIL SAVE'
