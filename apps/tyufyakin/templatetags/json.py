from django import template
from django.utils import simplejson
from tyufyakin.utils import json_encoder
from django.utils.safestring import mark_safe


register = template.Library()

@register.filter
def json(object):
    return mark_safe(simplejson.dumps(object, default=json_encoder))
