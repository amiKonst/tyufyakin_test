from django.conf.urls import patterns, include, url
from django.contrib import admin
from tyufyakin.views import *

admin.autodiscover()


urlpatterns = patterns('',
    url(r'^$', 'tyufyakin.views.main_page', name='main'),
    url(r'^add/$', 'tyufyakin.views.add_record'),
    url(r'^calc/$', 'tyufyakin.views.calc_record'),
    url(r'^admin/', include(admin.site.urls)),
)
