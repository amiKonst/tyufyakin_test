# coding=utf-8
import datetime
import re
from django.conf import settings
from django.db.models.query import ValuesQuerySet
from decimal import Decimal
import time


def time_it(method):
    """
    Принтит длительность выполнения функции
    """
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()

        print '%r %.8f sec' % (method.__name__, te-ts)
        return result

    return timed


def dict_ascii(obj):
    """
    Преобразовывает ключи дикта в ascii, чтобы его можно было использовать в keyword arguments
    """
    return dict((k.encode('ascii'), v) for (k, v) in obj.iteritems())


def json_encoder(obj):
    """
    Дополнительная логика для преобразования объектов JSON.
    Пример: json.dumps(obj, default=json_encoder)
    """
    if isinstance(obj, datetime.datetime):
        return obj.strftime(settings.DATETIME_FORMAT)
    elif isinstance(obj, datetime.date):
        return obj.strftime(settings.DATE_FORMAT)
    elif isinstance(obj, datetime.time):
        return obj.strftime(settings.TIME_FORMAT)
    elif type(obj) is ValuesQuerySet:
        return list(obj)
    elif type(obj) is Decimal:
        return float(obj)
    raise TypeError("%r is not JSON serializable (type: %s)" % (obj, type(obj)))


def set_cookie(response, key, value, days_expire=7):
    if days_expire is None:
        max_age = 365 * 24 * 60 * 60  # one year
    else:
        max_age = days_expire * 24 * 60 * 60
    expires = datetime.datetime.strftime(datetime.datetime.utcnow() + datetime.timedelta(seconds=max_age), "%a, %d-%b-%Y %H:%M:%S GMT")
    response.set_cookie(key, value, max_age=max_age, expires=expires, domain=settings.SESSION_COOKIE_DOMAIN, secure=settings.SESSION_COOKIE_SECURE or None)


def transliterate(string):
    """
    Функция транслитерации кириллици в латиницу
    """
    capital_letters = {u'А': u'A',
                       u'Б': u'B',
                       u'В': u'V',
                       u'Г': u'G',
                       u'Д': u'D',
                       u'Е': u'E',
                       u'Ё': u'E',
                       u'З': u'Z',
                       u'И': u'I',
                       u'Й': u'Y',
                       u'К': u'K',
                       u'Л': u'L',
                       u'М': u'M',
                       u'Н': u'N',
                       u'О': u'O',
                       u'П': u'P',
                       u'Р': u'R',
                       u'С': u'S',
                       u'Т': u'T',
                       u'У': u'U',
                       u'Ф': u'F',
                       u'Х': u'H',
                       u'Ъ': u'',
                       u'Ы': u'Y',
                       u'Ь': u'',
                       u'Э': u'E',}

    capital_letters_transliterated_to_multiple_letters = {u'Ж': u'Zh',
                                                          u'Ц': u'Ts',
                                                          u'Ч': u'Ch',
                                                          u'Ш': u'Sh',
                                                          u'Щ': u'Sch',
                                                          u'Ю': u'Yu',
                                                          u'Я': u'Ya',}


    lower_case_letters = {u'а': u'a',
                          u'б': u'b',
                          u'в': u'v',
                          u'г': u'g',
                          u'д': u'd',
                          u'е': u'e',
                          u'ё': u'e',
                          u'ж': u'zh',
                          u'з': u'z',
                          u'и': u'i',
                          u'й': u'y',
                          u'к': u'k',
                          u'л': u'l',
                          u'м': u'm',
                          u'н': u'n',
                          u'о': u'o',
                          u'п': u'p',
                          u'р': u'r',
                          u'с': u's',
                          u'т': u't',
                          u'у': u'u',
                          u'ф': u'f',
                          u'х': u'h',
                          u'ц': u'ts',
                          u'ч': u'ch',
                          u'ш': u'sh',
                          u'щ': u'sch',
                          u'ъ': u'',
                          u'ы': u'y',
                          u'ь': u'',
                          u'э': u'e',
                          u'ю': u'yu',
                          u'я': u'ya',}

    for cyrillic_string, latin_string in capital_letters_transliterated_to_multiple_letters.iteritems():
        string = re.sub(ur"%s([а-я])" % cyrillic_string, ur'%s\1' % latin_string, string)

    for dictionary in (capital_letters, lower_case_letters):

        for cyrillic_string, latin_string in dictionary.iteritems():
            string = string.replace(cyrillic_string, latin_string)

    for cyrillic_string, latin_string in capital_letters_transliterated_to_multiple_letters.iteritems():
        string = string.replace(cyrillic_string, latin_string.upper())

    return string


def nofollow_links(text):
    """
    Добавляет всем ссылкам в тексте атрибут nofollow, если ссылка ведет на внешний источник или изображение/документ
    """
    # TODO: Заменить список доменов на Sites.objects.all().values_list('domain', flat=True)
    domains = ['jelirai.ru', 'jelirai.prod.greensight.ru', 'jelirai.stages.greensight.ru']
    splitted = text.split('<a ')
    for i in xrange(1, len(splitted)):
        bits = splitted[i].split('>', 1)
        link_attrs = bits[0]
        #Если nofollow уже есть, то ничего не делаем
        if re.search('rel=[\'\"]nofollow[\'\"]', link_attrs):
            pass
        #Если изображение или документ, добавляем nofollow
        elif re.search('href=[\"\'].\S*\.(jpg|bmp|jpeg|gif|png|tif|pdf|doc|docx)[\"\']', link_attrs):
            link_attrs += ' rel="nofollow"'
        else:
            external = True
            for domain in domains:
                #Если ссылка внешняя, то добавляем nofollow
                if re.search('href=[\'\"]http\S*(%s).\S*[\'\"]' % domain, link_attrs):
                    external = False
                    break
            if external:
                link_attrs += ' rel="nofollow"'
        splitted[i] = '>'.join([link_attrs, bits[1]])
    result = '<a '.join(splitted)
    return result
