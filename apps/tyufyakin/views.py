# coding=utf-8
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.core.paginator import InvalidPage
from django.template import loader, RequestContext
from django.http import HttpResponse, HttpResponseServerError, HttpResponseRedirect, HttpResponseForbidden, HttpResponseNotFound
from django.db.models import Sum, Count, Avg, Max, Min
from django.views.generic.list import View, BaseListView, MultipleObjectMixin
from tyufyakin.models import *
from .tasks import *
from django.contrib.contenttypes.models import ContentType
from django.conf import settings
from django.db.models.fields.related import ForeignKey
from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators.csrf import csrf_exempt
from tyufyakin.utils import json_encoder
from django.shortcuts import redirect
import traceback
import requests, json
import json

RECORD_VALUES = ['id', 'first_summand', 'second_summand', 'result', 
    'last_modified_date', 'has_error' ]

def main_page(request):
    """
    Главную страницу.
    """
    records = Record.objects.order_by('-last_modified_date') \
        .values(*RECORD_VALUES)[:30]
    template = loader.get_template('index.html')

    context = {
        'records': list(records)
    }
    return HttpResponse(template.render(RequestContext(request, context)))


@csrf_exempt
def add_record(request):
    """
    Добавляем запись через ajax
    """
    data = request.POST.dict()
    record = Record.objects.create(
        first_summand=(data['first_summand']), 
        second_summand=(data['second_summand']))
    data = Record.objects.filter(id=record.id).values(*RECORD_VALUES)[0]
    return HttpResponse(json.dumps(data, default=json_encoder, ensure_ascii=False), mimetype='application/javascript')


def calc_record(request):
    """
    Запускаем функцию подсчета
    """
    # api_root = 'http://localhost:5555/api'
    # task_api = '{}/task'.format(api_root)

    # args = json.dumps({'args': []})
    # url = '{}/async-apply/tasks.assume'.format(task_api)
    # resp = requests.post(url, data=args)
    assume()
    return HttpResponseRedirect('/')
