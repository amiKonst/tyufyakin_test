# coding=utf-8
from sys import stdout
from subprocess import Popen, PIPE, STDOUT

CMD_LIST = [
    ['Updating system packages',  [
        'sudo apt-get -qq update',
        'sudo apt-get install --quiet --assume-yes git curl python-dev python-pip']],
    ['Updating fabric tools', [
        'if [ ! -f ~/get-pip.py ]; then %s; %s; fi' % (  # require latest pip
            'curl --silent -o ~/get-pip.py https://raw.github.com/pypa/pip/master/contrib/get-pip.py',
            'sudo python ~/get-pip.py'),
        'sudo pip install -q jinja2 git+git://github.com/greensight/fabtools.git']]]


def run_command(commands, description):
    if isinstance(commands, basestring):
        commands = [commands]

    puts(description + '...')
    for command in commands:
        proc = Popen(command,
                     stderr=STDOUT,
                     stdout=PIPE,
                     shell=True)

        return_code = proc.wait()
        if return_code != 0:
            puts(proc.stdout.read())
            break


def puts(string):
    stdout.write(string)
    stdout.flush()


def main():
    for descr, cmds in CMD_LIST:
        run_command(cmds, descr)

if __name__ == '__main__':
    main()
