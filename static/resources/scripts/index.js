var BaseVM = function(config) {
    var self = $.extend(this, config);

    self.objectsList = ko.observableArray(self.records || []);

    self.statusMessage = ko.observable('Добавим значения переменных');
    self.inpFirst = ko.observable('');
    self.inpSecond = ko.observable('');

    self.checkNumber = function(str){

        if (!(isNaN(parseFloat(str)))) {
            self.statusMessage('')
            return parseFloat(str)
        } else {
            if (str.length != 0) {
                self.statusMessage('Вводите только числа')
            }
            return str
        }
    }

    self.validateInp = function() {
        self.inpFirst(self.checkNumber(self.inpFirst()));
        self.inpSecond(self.checkNumber(self.inpSecond()));
    }

    self.addValue = function() {
        if ((self.inpFirst() == '') || (self.inpSecond() == '')) {
            self.statusMessage('Вы забыли ввести числа =)')
            return;
        }

        var requestData = new FormData();
        requestData.append('csrfmiddlewaretoken', $('input[name=csrfmiddlewaretoken]').val());
        requestData.append('first_summand', self.inpFirst());
        requestData.append('second_summand', self.inpSecond());

        $.ajax({
            url: '/add/',
            type: 'POST',
            data: requestData,
            processData: false,
            contentType: false,
            cache: false,
            enctype: 'multipart/form-data',
            statusCode: {
                200: function(response) {
                    self.objectsList.splice(0,0,JSON.parse(response.responseText));
                },
                500: function(data) {
                    self.statusMessage('Ошибка при сохранении')
                },
                413: function(response) {
                    self.statusMessage('Ошибка при сохранении')
                }
            }
        });
    }

    self.calcFun = function() {
        location.href = "/calc/"
    }
};
